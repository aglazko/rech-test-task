import json
import datetime
from pathlib import Path

import torch
import whisper

# move to config, hardcode for now
LOG_FILE = Path("log.json")
MODEL_SIZE = "medium"
DEVICE = "cpu"


def transcribe_audio(input_file: Path) -> str:
    model = whisper.load_model(MODEL_SIZE)
    model.to(torch.device(DEVICE))
    result = model.transcribe(str(input_file), condition_on_previous_text=True)
    save_results(transcription=result, filepath=input_file)
    return result["text"]


def save_results(transcription: dict, filepath: Path):
    log_history = []
    if LOG_FILE.exists():
        with open(LOG_FILE, 'r') as f:
            log_history = json.load(f)

    new_log_object = {
        "datetime": str(datetime.datetime.now()),
        "filepath": str(filepath),
        "language": transcription["language"],
        "text": transcription["text"],
    }
    log_history.append(new_log_object)

    with open(LOG_FILE, 'w') as f:
        json.dump(log_history, f)


if __name__ == "__main__":
    print(f"Loading {MODEL_SIZE} whisper model")
    model = whisper.load_model(MODEL_SIZE)
    print("Done")
