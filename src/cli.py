import argparse
from pathlib import Path

from src.utils import get_logger
from src.modification import modify_audio
from src.transcription import transcribe_audio

logger = get_logger(name="CLI")


def get_args():
    parser = argparse.ArgumentParser(description="wav file transcription and modification app")
    parser.add_argument("input_file", help="Path to wav file")
    parser.add_argument("-t", "--transcribe", action='store_true', help="Transcribe audio to text")
    parser.add_argument("-s", "--speed", type=float, default=1.0, help="Target audio speed, [0.5, inf) (default 1.0)")
    parser.add_argument("-v", "--volume", type=int, default=0, help="Volum increase / decrease, dB (default 0)")
    parser.add_argument("-i", "--inplace", action='store_true', help="Modify file inplace instead of creating new one")
    parser.add_argument("-o", "--output", type=str, default=None, help="Output file path")
    args = parser.parse_args()
    return args


def verify_args(args) -> bool:
    input_file = Path(args.input_file)
    if not input_file.is_file():
        logger.error(f"File not found: {input_file}")
        return False
    if input_file.suffix != ".wav":
        logger.error(f"FileExtentionError: {input_file.suffix} file was floud. Only .wav files is supported as input")
        return False

    if args.output:
        output_file = Path(args.output)
        if args.inplace:
            logger.error("Output file was determined while inplace option is active. Choose only one option")
            return False
        if output_file.suffix != ".wav":
            logger.error(f"FileExtentionError: {output_file.suffix} file was floud. Only .wav files is supported as output")
            return False

    if args.speed < 0.5:
        logger.error("ValueError: audio speed can`t be less then 0.5")
        return False

    return True


def main():
    args = get_args()
    if not verify_args(args):
        logger.error("Arguments verification failed. See logs for details")
        return

    input_file = Path(args.input_file)
    modify_audio(
        input_file=input_file,
        output_file=args.output,
        speed=args.speed,
        volume=args.volume,
        inplace=args.inplace,
    )

    if args.transcribe:
        logger.info("Transcribing...")
        transcription = transcribe_audio(input_file)
        print(f"Transcription:\n{transcription}")

    logger.info("Done")


if __name__ == "__main__":
    main()
