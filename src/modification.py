from pathlib import Path

from pydub import AudioSegment

from src.utils import get_logger
from pydub.utils import make_chunks, register_pydub_effect

logger = get_logger(name="modification")


def modify_audio(input_file: Path, output_file: Path = None, speed=1.0, volume=0, inplace=False):
    if speed == 1.0 and volume == 0:
        logger.info("Skip modification")
        return

    output_file = get_output_file_name(
        input_file=input_file,
        output_file=output_file,
        speed=speed,
        volume=volume,
        inplace=inplace,
    )

    original_audio = AudioSegment.from_file(str(input_file))
    precessed_audio = change_speed(sound=original_audio, speed=speed)
    precessed_audio = precessed_audio + volume
    precessed_audio.export(str(output_file), format="wav")

    volume_sign = '-' if volume < 0 else '+'
    logger.info(f"""Modification arguments:
        Speed: {speed}
        Volume: {volume_sign}{abs(volume)} dB
        Modifyed file: {output_file}""")


def change_speed(sound: AudioSegment, speed=1.0):
    if speed > 1:
        return sound.speedup(speed)
    return sound.speeddown(playback_speed=speed)


def get_output_file_name(input_file: Path, output_file: Path = None, speed=1.0, volume=0, inplace=False) -> Path:
    if output_file:
        return Path(output_file)
    if inplace:
        return input_file
    new_filename = f"{input_file.stem}_s{speed}_v{volume}{input_file.suffix}"
    return input_file.parent / new_filename


@register_pydub_effect
def speeddown(seg, playback_speed=0.75, chunk_size=10, crossfade=2):
    assert 0.5 <= playback_speed <= 1
    if playback_speed == 1:
        return seg

    repeat_every = round(playback_speed / (1 - playback_speed))

    chunks = make_chunks(seg, chunk_size)
    if len(chunks) < 2:
        raise Exception("Could not speed down AudioSegment, it was too short {2:0.2f}s for the current settings:\n{0}ms chunks at {1:0.1f}x speedup".format(
            chunk_size, playback_speed, seg.duration_seconds))

    out = chunks[0]
    for idx, chunk in enumerate(chunks[1:-1]):
        out = out.append(chunk, crossfade=None)
        if idx % repeat_every == 0:
            out = out.append(chunk, crossfade=crossfade)

    out += chunks[-1]
    return out
